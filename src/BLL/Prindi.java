/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author diona
 */
@Entity
@Table(name = "Prindi")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Prindi.findAll", query = "SELECT p FROM Prindi p")
    , @NamedQuery(name = "Prindi.findByPid", query = "SELECT p FROM Prindi p WHERE p.pid = :pid")
    , @NamedQuery(name = "Prindi.findByEmri", query = "SELECT p FROM Prindi p WHERE p.emri = :emri")
    , @NamedQuery(name = "Prindi.findByMbiemri", query = "SELECT p FROM Prindi p WHERE p.mbiemri = :mbiemri")
    , @NamedQuery(name = "Prindi.findByNrTel", query = "SELECT p FROM Prindi p WHERE p.nrTel = :nrTel")
    , @NamedQuery(name = "Prindi.findByEmail", query = "SELECT p FROM Prindi p WHERE p.email = :email")})
public class Prindi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
        @GeneratedValue(generator="InvSeq")
@SequenceGenerator(name="InvSeq",sequenceName="INV_SEQ", allocationSize=1)
    @Column(name = "PID")
    private Integer pid;
    @Basic(optional = false)
    @Column(name = "Emri")
    private String emri;
    @Basic(optional = false)
    @Column(name = "Mbiemri")
    private String mbiemri;
    @Basic(optional = false)
    @Column(name = "NrTel")
    private int nrTel;
    @Basic(optional = false)
    @Column(name = "Email")
    private String email;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pid")
    private Collection<KontrataPrindi> kontrataPrindiCollection;
    @JoinColumn(name = "Gjinia", referencedColumnName = "GjiniaID")
    @ManyToOne(optional = false)
    private Gjinia gjinia;

    public Prindi() {
    }

    public Prindi(Integer pid) {
        this.pid = pid;
    }

    public Prindi(Integer pid, String emri, String mbiemri, int nrTel, String email) {
        this.pid = pid;
        this.emri = emri;
        this.mbiemri = mbiemri;
        this.nrTel = nrTel;
        this.email = email;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getEmri() {
        return emri;
    }

    public void setEmri(String emri) {
        this.emri = emri;
    }

    public String getMbiemri() {
        return mbiemri;
    }

    public void setMbiemri(String mbiemri) {
        this.mbiemri = mbiemri;
    }

    public int getNrTel() {
        return nrTel;
    }

    public void setNrTel(int nrTel) {
        this.nrTel = nrTel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @XmlTransient
    public Collection<KontrataPrindi> getKontrataPrindiCollection() {
        return kontrataPrindiCollection;
    }

    public void setKontrataPrindiCollection(Collection<KontrataPrindi> kontrataPrindiCollection) {
        this.kontrataPrindiCollection = kontrataPrindiCollection;
    }

    public Gjinia getGjinia() {
        return gjinia;
    }

    public void setGjinia(Gjinia gjinia) {
        this.gjinia = gjinia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pid != null ? pid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Prindi)) {
            return false;
        }
        Prindi other = (Prindi) object;
        if ((this.pid == null && other.pid != null) || (this.pid != null && !this.pid.equals(other.pid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.Prindi[ pid=" + pid + " ]";
    }
    
}
