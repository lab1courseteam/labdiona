package BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "Planifikimi")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Planifikimi.findAll", query = "SELECT p FROM Planifikimi p")
    , @NamedQuery(name = "Planifikimi.findByPlanifikimiID", query = "SELECT p FROM Planifikimi p WHERE p.planifikimiID = :planifikimiID")})
public class Planifikimi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(generator = "InvSeq")
    @SequenceGenerator(name = "InvSeq", sequenceName = "INV_SEQ", allocationSize = 1)
    @Column(name = "PlanifikimiID")
    private Integer planifikimiID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "planifikimiID")
    private Collection<Enrollment> enrollmentCollection;

    public Planifikimi() {
    }

    public Planifikimi(Integer planifikimiID) {
        this.planifikimiID = planifikimiID;
    }

    public Integer getPlanifikimiID() {
        return planifikimiID;
    }

    public void setPlanifikimiID(Integer planifikimiID) {
        this.planifikimiID = planifikimiID;
    }

    @XmlTransient
    public Collection<Enrollment> getEnrollmentCollection() {
        return enrollmentCollection;
    }

    public void setEnrollmentCollection(Collection<Enrollment> enrollmentCollection) {
        this.enrollmentCollection = enrollmentCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (planifikimiID != null ? planifikimiID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Planifikimi)) {
            return false;
        }
        Planifikimi other = (Planifikimi) object;
        if ((this.planifikimiID == null && other.planifikimiID != null) || (this.planifikimiID != null && !this.planifikimiID.equals(other.planifikimiID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.Planifikimi[ planifikimiID=" + planifikimiID + " ]";
    }

}
