/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author diona
 */
@Entity
@Table(name = "KontrataPrindi")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "KontrataPrindi.findAll", query = "SELECT k FROM KontrataPrindi k")
    , @NamedQuery(name = "KontrataPrindi.findByKpid", query = "SELECT k FROM KontrataPrindi k WHERE k.kpid = :kpid")})
public class KontrataPrindi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "KPID")
    private Integer kpid;
    @JoinColumn(name = "KID", referencedColumnName = "KID")
    @ManyToOne(optional = false)
    private Kontrata kid;
    @JoinColumn(name = "PID", referencedColumnName = "PID")
    @ManyToOne(optional = false)
    private Prindi pid;

    public KontrataPrindi() {
    }

    public KontrataPrindi(Integer kpid) {
        this.kpid = kpid;
    }

    public Integer getKpid() {
        return kpid;
    }

    public void setKpid(Integer kpid) {
        this.kpid = kpid;
    }

    public Kontrata getKid() {
        return kid;
    }

    public void setKid(Kontrata kid) {
        this.kid = kid;
    }

    public Prindi getPid() {
        return pid;
    }

    public void setPid(Prindi pid) {
        this.pid = pid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kpid != null ? kpid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KontrataPrindi)) {
            return false;
        }
        KontrataPrindi other = (KontrataPrindi) object;
        if ((this.kpid == null && other.kpid != null) || (this.kpid != null && !this.kpid.equals(other.kpid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.KontrataPrindi[ kpid=" + kpid + " ]";
    }
    
}
