/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author diona
 */
@Entity
@Table(name = "LogIn")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LogIn.findAll", query = "SELECT l FROM LogIn l")
    , @NamedQuery(name = "LogIn.findByLogInID", query = "SELECT l FROM LogIn l WHERE l.logInID = :logInID")
    , @NamedQuery(name = "LogIn.findByUsername", query = "SELECT l FROM LogIn l WHERE l.username = :username")
    , @NamedQuery(name = "LogIn.findByPassword", query = "SELECT l FROM LogIn l WHERE l.password = :password")})
public class LogIn implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "LogInID")
    private Integer logInID;
    @Column(name = "Username")
    private String username;
    @Column(name = "Password")
    private String password;
    @JoinColumn(name = "RoliID", referencedColumnName = "RoliID")
    @ManyToOne(optional = false)
    private Roli roliID;

    public LogIn() {
    }

    public LogIn(Integer logInID) {
        this.logInID = logInID;
    }

    public Integer getLogInID() {
        return logInID;
    }

    public void setLogInID(Integer logInID) {
        this.logInID = logInID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Roli getRoliID() {
        return roliID;
    }

    public void setRoliID(Roli roliID) {
        this.roliID = roliID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (logInID != null ? logInID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogIn)) {
            return false;
        }
        LogIn other = (LogIn) object;
        if ((this.logInID == null && other.logInID != null) || (this.logInID != null && !this.logInID.equals(other.logInID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.LogIn[ logInID=" + logInID + " ]";
    }
    
}
