/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author diona
 */
@Entity
@Table(name = "Shteti")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Shteti.findAll", query = "SELECT s FROM Shteti s")
    , @NamedQuery(name = "Shteti.findByShID", query = "SELECT s FROM Shteti s WHERE s.shID = :shID")
    , @NamedQuery(name = "Shteti.findByEmri", query = "SELECT s FROM Shteti s WHERE s.emri = :emri")
    , @NamedQuery(name = "Shteti.findByKodiPostar", query = "SELECT s FROM Shteti s WHERE s.kodiPostar = :kodiPostar")})
public class Shteti implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ShID")
    private Integer shID;
    @Basic(optional = false)
    @Column(name = "Emri")
    private String emri;
    @Basic(optional = false)
    @Column(name = "Kodi Postar")
    private String kodiPostar;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "shteti")
    private Collection<Mesuesja> mesuesjaCollection;

    public Shteti() {
    }

    public Shteti(Integer shID) {
        this.shID = shID;
    }

    public Shteti(Integer shID, String emri, String kodiPostar) {
        this.shID = shID;
        this.emri = emri;
        this.kodiPostar = kodiPostar;
    }

    public Integer getShID() {
        return shID;
    }

    public void setShID(Integer shID) {
        this.shID = shID;
    }

    public String getEmri() {
        return emri;
    }

    public void setEmri(String emri) {
        this.emri = emri;
    }

    public String getKodiPostar() {
        return kodiPostar;
    }

    public void setKodiPostar(String kodiPostar) {
        this.kodiPostar = kodiPostar;
    }

    @XmlTransient
    public Collection<Mesuesja> getMesuesjaCollection() {
        return mesuesjaCollection;
    }

    public void setMesuesjaCollection(Collection<Mesuesja> mesuesjaCollection) {
        this.mesuesjaCollection = mesuesjaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (shID != null ? shID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Shteti)) {
            return false;
        }
        Shteti other = (Shteti) object;
        if ((this.shID == null && other.shID != null) || (this.shID != null && !this.shID.equals(other.shID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.Shteti[ shID=" + shID + " ]";
    }
    
}
