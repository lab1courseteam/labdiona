/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@Table(name = "Drejtimi")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Drejtimi.findAll", query = "SELECT d FROM Drejtimi d")
    , @NamedQuery(name = "Drejtimi.findByDrejtimiID", query = "SELECT d FROM Drejtimi d WHERE d.drejtimiID = :drejtimiID")
    , @NamedQuery(name = "Drejtimi.findByDrejtimi", query = "SELECT d FROM Drejtimi d WHERE d.drejtimi = :drejtimi")})
public class Drejtimi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "DrejtimiID")
    private Integer drejtimiID;
    @Basic(optional = false)
    @Column(name = "Drejtimi")
    private String drejtimi;
    @JoinColumn(name = "FakultetiID", referencedColumnName = "FtID")
    @ManyToOne(optional = false)
    private Fakulteti fakultetiID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "drejtimiID")
    private Collection<Niveli> niveliCollection;

    public Drejtimi() {
    }

    public Drejtimi(Integer drejtimiID) {
        this.drejtimiID = drejtimiID;
    }

    public Drejtimi(Integer drejtimiID, String drejtimi) {
        this.drejtimiID = drejtimiID;
        this.drejtimi = drejtimi;
    }

    public Integer getDrejtimiID() {
        return drejtimiID;
    }

    public void setDrejtimiID(Integer drejtimiID) {
        this.drejtimiID = drejtimiID;
    }

    public String getDrejtimi() {
        return drejtimi;
    }

    public void setDrejtimi(String drejtimi) {
        this.drejtimi = drejtimi;
    }

    public Fakulteti getFakultetiID() {
        return fakultetiID;
    }

    public void setFakultetiID(Fakulteti fakultetiID) {
        this.fakultetiID = fakultetiID;
    }

    @XmlTransient
    public Collection<Niveli> getNiveliCollection() {
        return niveliCollection;
    }

    public void setNiveliCollection(Collection<Niveli> niveliCollection) {
        this.niveliCollection = niveliCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (drejtimiID != null ? drejtimiID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Drejtimi)) {
            return false;
        }
        Drejtimi other = (Drejtimi) object;
        if ((this.drejtimiID == null && other.drejtimiID != null) || (this.drejtimiID != null && !this.drejtimiID.equals(other.drejtimiID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.Drejtimi[ drejtimiID=" + drejtimiID + " ]";
    }
    
}
