/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author diona
 */
@Entity
@Table(name = "Shendeti")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Shendeti.findAll", query = "SELECT s FROM Shendeti s")
    , @NamedQuery(name = "Shendeti.findByShID", query = "SELECT s FROM Shendeti s WHERE s.shID = :shID")
    , @NamedQuery(name = "Shendeti.findByShendeti", query = "SELECT s FROM Shendeti s WHERE s.shendeti = :shendeti")
    , @NamedQuery(name = "Shendeti.findByTerapia", query = "SELECT s FROM Shendeti s WHERE s.terapia = :terapia")})
public class Shendeti implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ShID")
    private Integer shID;
    @Basic(optional = false)
    @Column(name = "Shendeti")
    private String shendeti;
    @Column(name = "Terapia")
    private String terapia;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "shendeti")
    private Collection<Femiu> femiuCollection;

    public Shendeti() {
    }

    public Shendeti(Integer shID) {
        this.shID = shID;
    }

    public Shendeti(Integer shID, String shendeti) {
        this.shID = shID;
        this.shendeti = shendeti;
    }

    public Integer getShID() {
        return shID;
    }

    public void setShID(Integer shID) {
        this.shID = shID;
    }

    public String getShendeti() {
        return shendeti;
    }

    public void setShendeti(String shendeti) {
        this.shendeti = shendeti;
    }

    public String getTerapia() {
        return terapia;
    }

    public void setTerapia(String terapia) {
        this.terapia = terapia;
    }

    @XmlTransient
    public Collection<Femiu> getFemiuCollection() {
        return femiuCollection;
    }

    public void setFemiuCollection(Collection<Femiu> femiuCollection) {
        this.femiuCollection = femiuCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (shID != null ? shID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Shendeti)) {
            return false;
        }
        Shendeti other = (Shendeti) object;
        if ((this.shID == null && other.shID != null) || (this.shID != null && !this.shID.equals(other.shID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.Shendeti[ shID=" + shID + " ]";
    }
    
}
