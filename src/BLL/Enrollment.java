/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author diona
 */
@Entity
@Table(name = "Enrollment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Enrollment.findAll", query = "SELECT e FROM Enrollment e")
    , @NamedQuery(name = "Enrollment.findByEid", query = "SELECT e FROM Enrollment e WHERE e.eid = :eid")})
public class Enrollment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "EID")
    private Integer eid;
    @JoinColumn(name = "GrupiID", referencedColumnName = "GrupiID")
    @ManyToOne(optional = false)
    private Grupi grupiID;
    @JoinColumn(name = "MID", referencedColumnName = "MID")
    @ManyToOne(optional = false)
    private Mesuesja mid;
    @JoinColumn(name = "PlanifikimiID", referencedColumnName = "PlanifikimiID")
    @ManyToOne(optional = false)
    private Planifikimi planifikimiID;

    public Enrollment() {
    }

    public Enrollment(Integer eid) {
        this.eid = eid;
    }

    public Integer getEid() {
        return eid;
    }

    public void setEid(Integer eid) {
        this.eid = eid;
    }

    public Grupi getGrupiID() {
        return grupiID;
    }

    public void setGrupiID(Grupi grupiID) {
        this.grupiID = grupiID;
    }

    public Mesuesja getMid() {
        return mid;
    }

    public void setMid(Mesuesja mid) {
        this.mid = mid;
    }

    public Planifikimi getPlanifikimiID() {
        return planifikimiID;
    }

    public void setPlanifikimiID(Planifikimi planifikimiID) {
        this.planifikimiID = planifikimiID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (eid != null ? eid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Enrollment)) {
            return false;
        }
        Enrollment other = (Enrollment) object;
        if ((this.eid == null && other.eid != null) || (this.eid != null && !this.eid.equals(other.eid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.Enrollment[ eid=" + eid + " ]";
    }
    
}
