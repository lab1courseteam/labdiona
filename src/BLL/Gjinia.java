/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author diona
 */
@Entity
@Table(name = "Gjinia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Gjinia.findAll", query = "SELECT g FROM Gjinia g")
    , @NamedQuery(name = "Gjinia.findByGjiniaID", query = "SELECT g FROM Gjinia g WHERE g.gjiniaID = :gjiniaID")
    , @NamedQuery(name = "Gjinia.findByGjinia", query = "SELECT g FROM Gjinia g WHERE g.gjinia = :gjinia")})
public class Gjinia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "GjiniaID")
    private Integer gjiniaID;
    @Basic(optional = false)
    @Column(name = "Gjinia")
    private String gjinia;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gjinia")
    private Collection<Mesuesja> mesuesjaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gjinia")
    private Collection<Femiu> femiuCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gjinia")
    private Collection<Prindi> prindiCollection;

    public Gjinia() {
    }

    public Gjinia(Integer gjiniaID) {
        this.gjiniaID = gjiniaID;
    }

    public Gjinia(Integer gjiniaID, String gjinia) {
        this.gjiniaID = gjiniaID;
        this.gjinia = gjinia;
    }

    public Integer getGjiniaID() {
        return gjiniaID;
    }

    public void setGjiniaID(Integer gjiniaID) {
        this.gjiniaID = gjiniaID;
    }

    public String getGjinia() {
        return gjinia;
    }

    public void setGjinia(String gjinia) {
        this.gjinia = gjinia;
    }

    @XmlTransient
    public Collection<Mesuesja> getMesuesjaCollection() {
        return mesuesjaCollection;
    }

    public void setMesuesjaCollection(Collection<Mesuesja> mesuesjaCollection) {
        this.mesuesjaCollection = mesuesjaCollection;
    }

    @XmlTransient
    public Collection<Femiu> getFemiuCollection() {
        return femiuCollection;
    }

    public void setFemiuCollection(Collection<Femiu> femiuCollection) {
        this.femiuCollection = femiuCollection;
    }

    @XmlTransient
    public Collection<Prindi> getPrindiCollection() {
        return prindiCollection;
    }

    public void setPrindiCollection(Collection<Prindi> prindiCollection) {
        this.prindiCollection = prindiCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gjiniaID != null ? gjiniaID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gjinia)) {
            return false;
        }
        Gjinia other = (Gjinia) object;
        if ((this.gjiniaID == null && other.gjiniaID != null) || (this.gjiniaID != null && !this.gjiniaID.equals(other.gjiniaID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.Gjinia[ gjiniaID=" + gjiniaID + " ]";
    }
    
}
