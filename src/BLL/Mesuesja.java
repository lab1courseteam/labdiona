/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author diona
 */
@Entity
@Table(name = "Mesuesja")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mesuesja.findAll", query = "SELECT m FROM Mesuesja m")
    , @NamedQuery(name = "Mesuesja.findByMid", query = "SELECT m FROM Mesuesja m WHERE m.mid = :mid")
    , @NamedQuery(name = "Mesuesja.findByEmri", query = "SELECT m FROM Mesuesja m WHERE m.emri = :emri")
    , @NamedQuery(name = "Mesuesja.findByMbiemri", query = "SELECT m FROM Mesuesja m WHERE m.mbiemri = :mbiemri")})
public class Mesuesja implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "MID")
    private Integer mid;
    @Basic(optional = false)
    @Column(name = "Emri")
    private String emri;
    @Basic(optional = false)
    @Column(name = "Mbiemri")
    private String mbiemri;
    @JoinColumn(name = "Gjinia", referencedColumnName = "GjiniaID")
    @ManyToOne(optional = false)
    private Gjinia gjinia;
    @JoinColumn(name = "Qyteti", referencedColumnName = "QID")
    @ManyToOne(optional = false)
    private Qyteti qyteti;
    @JoinColumn(name = "Shteti", referencedColumnName = "ShID")
    @ManyToOne(optional = false)
    private Shteti shteti;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mid")
    private Collection<Enrollment> enrollmentCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mid")
    private Collection<MesuejaKontrate> mesuejaKontrateCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mesuesjaID")
    private Collection<Niveli> niveliCollection;

    public Mesuesja() {
    }

    public Mesuesja(Integer mid) {
        this.mid = mid;
    }

    public Mesuesja(Integer mid, String emri, String mbiemri) {
        this.mid = mid;
        this.emri = emri;
        this.mbiemri = mbiemri;
    }

    public Integer getMid() {
        return mid;
    }

    public void setMid(Integer mid) {
        this.mid = mid;
    }

    public String getEmri() {
        return emri;
    }

    public void setEmri(String emri) {
        this.emri = emri;
    }

    public String getMbiemri() {
        return mbiemri;
    }

    public void setMbiemri(String mbiemri) {
        this.mbiemri = mbiemri;
    }

    public Gjinia getGjinia() {
        return gjinia;
    }

    public void setGjinia(Gjinia gjinia) {
        this.gjinia = gjinia;
    }

    public Qyteti getQyteti() {
        return qyteti;
    }

    public void setQyteti(Qyteti qyteti) {
        this.qyteti = qyteti;
    }

    public Shteti getShteti() {
        return shteti;
    }

    public void setShteti(Shteti shteti) {
        this.shteti = shteti;
    }

    @XmlTransient
    public Collection<Enrollment> getEnrollmentCollection() {
        return enrollmentCollection;
    }

    public void setEnrollmentCollection(Collection<Enrollment> enrollmentCollection) {
        this.enrollmentCollection = enrollmentCollection;
    }

    @XmlTransient
    public Collection<MesuejaKontrate> getMesuejaKontrateCollection() {
        return mesuejaKontrateCollection;
    }

    public void setMesuejaKontrateCollection(Collection<MesuejaKontrate> mesuejaKontrateCollection) {
        this.mesuejaKontrateCollection = mesuejaKontrateCollection;
    }

    @XmlTransient
    public Collection<Niveli> getNiveliCollection() {
        return niveliCollection;
    }

    public void setNiveliCollection(Collection<Niveli> niveliCollection) {
        this.niveliCollection = niveliCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mid != null ? mid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mesuesja)) {
            return false;
        }
        Mesuesja other = (Mesuesja) object;
        if ((this.mid == null && other.mid != null) || (this.mid != null && !this.mid.equals(other.mid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.Mesuesja[ mid=" + mid + " ]";
    }
    
}
