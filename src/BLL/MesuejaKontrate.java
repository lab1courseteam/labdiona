/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author diona
 */
@Entity
@Table(name = "MesuejaKontrate")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MesuejaKontrate.findAll", query = "SELECT m FROM MesuejaKontrate m")
    , @NamedQuery(name = "MesuejaKontrate.findByMkID", query = "SELECT m FROM MesuejaKontrate m WHERE m.mkID = :mkID")})
public class MesuejaKontrate implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "MkID")
    private Integer mkID;
    @JoinColumn(name = "KID", referencedColumnName = "KID")
    @ManyToOne(optional = false)
    private Kontrata kid;
    @JoinColumn(name = "MID", referencedColumnName = "MID")
    @ManyToOne(optional = false)
    private Mesuesja mid;

    public MesuejaKontrate() {
    }

    public MesuejaKontrate(Integer mkID) {
        this.mkID = mkID;
    }

    public Integer getMkID() {
        return mkID;
    }

    public void setMkID(Integer mkID) {
        this.mkID = mkID;
    }

    public Kontrata getKid() {
        return kid;
    }

    public void setKid(Kontrata kid) {
        this.kid = kid;
    }

    public Mesuesja getMid() {
        return mid;
    }

    public void setMid(Mesuesja mid) {
        this.mid = mid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mkID != null ? mkID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MesuejaKontrate)) {
            return false;
        }
        MesuejaKontrate other = (MesuejaKontrate) object;
        if ((this.mkID == null && other.mkID != null) || (this.mkID != null && !this.mkID.equals(other.mkID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.MesuejaKontrate[ mkID=" + mkID + " ]";
    }
    
}
