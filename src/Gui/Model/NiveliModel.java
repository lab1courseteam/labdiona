
package Gui.Model;

import BLL.Niveli;
import java.util.List;


public class NiveliModel {
    private final String [] columnNames = {"NiveliID","Niveli","DrejtimiID","MesuesjaID"};
    private List <Niveli> data;
    public NiveliModel (List<Niveli>data){
        this.data = data;
    }
     public NiveliModel () {
    }
    public void add(List<Niveli>data){
        this.data = data;
    }
    public int getRowCount() {
        return data.size();
    }
    public int getColumnCount() {
        return columnNames.length;
    }
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public Niveli getPrindi (int index){
        return data.get(index);
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Niveli en = (Niveli)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return en.getNiveliID();
            case 1:
                return en.getNiveli();
            case 2:
                return en.getDrejtimiID();
            case 3:
                return en.getMesuesjaID();
                default:
                
                return null;
        }
    }

    public void fireTableDataChanged() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Niveli getNiveli(int row) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
