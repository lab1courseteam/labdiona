
package Gui.Model;

import BLL.Planifikimi;
import java.util.List;
import javax.swing.table.AbstractTableModel;


public class PlanifikimiModel   extends AbstractTableModel {
     private final String [] columnNames = {"PlanifikimiID"};
    private List <Planifikimi> data;
    public PlanifikimiModel (List<Planifikimi>data){
        this.data = data;
    }
     public PlanifikimiModel () {
    }
    public void add(List<Planifikimi>data){
        this.data = data;
    }
    public int getRowCount() {
        return data.size();
    }
    public int getColumnCount() {
        return columnNames.length;
    }
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public Planifikimi getPlanifikimi (int index){
        return data.get(index);
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Planifikimi en = (Planifikimi)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return en.getPlanifikimiID();
                default:
                
                return null;
        }
    }

    public void fireTableDataChanged() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Planifikimi Planifikimi(int row) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
