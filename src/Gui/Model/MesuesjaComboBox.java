
package Gui.Model;

import BLL.Mesuesja;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

public class MesuesjaComboBox extends AbstractListModel<Mesuesja> implements ComboBoxModel<Mesuesja> {
    
    private List<Mesuesja> data;
    private Mesuesja selectedItem;

    public MesuesjaComboBox(List<Mesuesja> data) {
        this.data = data;
    }

    public MesuesjaComboBox() {
    }

    public void add(List<Mesuesja> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Mesuesja getElementAt(int index) {
        return data.get(index);
    }

    public void setSelectedItem(Object anItem) {
        selectedItem = (Mesuesja) anItem;
    }

    public Object getSelectedItem() {
        return selectedItem;
    }
}
