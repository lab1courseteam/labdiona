package Gui.View;

import BLL.Mesuesja;
import BLL.Niveli;
import DAL.CrudFormException;
import DAL.NiveliInterface;
import DAL.NiveliRepository;
import Gui.Model.MesuesjaComboBox;
import Gui.Model.NiveliModel;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;

public class NiveliForm extends javax.swing.JInternalFrame {

    NiveliInterface ni = new NiveliRepository();
    NiveliModel nm = new NiveliModel();
    NiveliRepository nr = new NiveliRepository();
    MesuesjaComboBox mcb;

    public NiveliForm() {
        this.mcb=new MesuesjaComboBox();
        initComponents();
        loadTable();
        tabelaSelectedIndexChange();
        loadjComboBox2();
        
    }
    public void loadjComboBox2(){
        List<Mesuesja> lista = nr.findAll();
        mcb.add(lista);
        jComboBox2.setModel(mcb);
        jComboBox2.repaint();
        
        
    
    }

    public void loadTable() {
        try {
            List<Niveli> lista = ni.findAll();
            nm.add(lista);
            tabela.setModel((TableModel) nm);
            nm.fireTableDataChanged();
        } catch (CrudFormException ene) {
            JOptionPane.showMessageDialog(this, ene.getMessage());
        }
    }
     private void tabelaSelectedIndexChange() {
        final ListSelectionModel rowSM = tabela.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent Ise) {
                if (Ise.getValueIsAdjusting()) {
                    return;
                }
                ListSelectionModel rowSM = (ListSelectionModel) Ise.getSource();
                int selectedIndex = rowSM.getAnchorSelectionIndex();
                if (selectedIndex > -1) {
                    Niveli en = nm.getNiveli(selectedIndex);

                    txtNivelId.setText(en.getNiveliID()+ "");
                    txtNivel.setText(en.getNiveli());
                    mcb.setSelectedItem(en.getMesuesjaID());
                    jComboBox2.repaint();
                
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtnivelid = new javax.swing.JLabel();
        txtniveli = new javax.swing.JLabel();
        txtdrejtimiid = new javax.swing.JLabel();
        txtmesuesjaid = new javax.swing.JLabel();
        txtNivelId = new javax.swing.JTextField();
        txtNivel = new javax.swing.JTextField();
        btnSave = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabela = new javax.swing.JTable();
        Combobox1 = new javax.swing.JComboBox<>();
        jComboBox2 = new javax.swing.JComboBox<>();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);

        txtnivelid.setText("NiveliID");

        txtniveli.setText("Niveli");

        txtdrejtimiid.setText("DrejtimiID");

        txtmesuesjaid.setText("MesuesjaID");

        txtNivel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNivelActionPerformed(evt);
            }
        });

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        tabela.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "NiveliID", "Niveli", "DrejtimiID", "MesuesjaID"
            }
        ));
        jScrollPane1.setViewportView(tabela);

        Combobox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtmesuesjaid)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtdrejtimiid, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtnivelid)
                                            .addComponent(txtniveli))
                                        .addGap(11, 11, 11)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(39, 39, 39)
                                                .addComponent(txtNivelId, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(Combobox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(txtNivel, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addGap(50, 50, 50)
                                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnDelete)))
                        .addGap(0, 28, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNivelId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtnivelid))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNivel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtniveli))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSave)
                        .addComponent(btnClear)
                        .addComponent(btnEdit)
                        .addComponent(btnDelete))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtdrejtimiid)
                        .addComponent(Combobox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtmesuesjaid)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        // TODO add your handling code here:
         clearField();
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        // TODO add your handling code here:
         int row = tabela.getSelectedRow();
        if (row != -1) {
            Object[] ob = {"Po", "Jo"};
            int i = 
			JOptionPane.showOptionDialog(this, "A dëshironi ta fshini ?", "Fshirja", 
			JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE, null, ob, ob[1]);
            if (i == 0) {
                Niveli en = nm.getNiveli(row);
                try {
                    ni.delete(en);
                } catch (CrudFormException ex) {
                    Logger.getLogger(NiveliForm.class.getName()).log(Level.SEVERE, null, ex);
                }
                clearField();
                loadTable();
            } else {
                clearField();
            }
        }else{
            JOptionPane.showMessageDialog(this, "Nuk keni selektuar asgje per te fshire!");
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        // TODO add your handling code here:
         int row = tabela.getSelectedRow();
        if (!txtNivel.getText().equals("")) {
            if (row == -1) {
                Niveli en = new Niveli();
                en.setNiveliID(Integer.parseInt(txtnivelid.getText()));
                en.setNiveli(txtniveli.getText());
               
                try {
                    ni.create(en);
                } catch (CrudFormException ene) {
                    JOptionPane.showMessageDialog(this, ene.getMessage());
                }

            } else {
                Niveli en = nm.getNiveli(row);
                en.setNiveli(txtNivel.getText());

                try {
                    ni.edit(en);
                } catch (CrudFormException ex) {
                    Logger.getLogger(PrindiForm.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            clearField();
            loadTable();
        } else {
            JOptionPane.showMessageDialog(this, "Ju lutem plotesoni fushat obligative (me shenjen *)!");

        }
    }//GEN-LAST:event_btnEditActionPerformed

    private void txtNivelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNivelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNivelActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
        int row = tabela.getSelectedRow();
        if (!txtNivel.getText().equals("")) {
            if (row == -1) {
                Niveli en = new Niveli();
                //en.setPid(Integer.parseInt(txtId.getText()));
                en.setNiveli(txtNivel.getText());
                
               
               
                try {
                    ni.create(en);
                } catch (CrudFormException ene) {
                    JOptionPane.showMessageDialog(this, ene.getMessage());
                }

            } else {
                Niveli en = nm.getNiveli(row);
                en.setNiveli(txtNivel.getText());

                try {
                    ni.edit(en);
                } catch (CrudFormException ex) {
                    Logger.getLogger(PrindiForm.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            clearField();
            loadTable();
        } else {
            JOptionPane.showMessageDialog(this, "Ju lutem plotesoni fushat obligative (me shenjen *)!");

        }
        
        
    }//GEN-LAST:event_btnSaveActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> Combobox1;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabela;
    private javax.swing.JTextField txtNivel;
    private javax.swing.JTextField txtNivelId;
    private javax.swing.JLabel txtdrejtimiid;
    private javax.swing.JLabel txtmesuesjaid;
    private javax.swing.JLabel txtniveli;
    private javax.swing.JLabel txtnivelid;
    // End of variables declaration//GEN-END:variables
 private void clearField() {
        txtNivelId.setText("");
        txtNivel.setText("");
       // txtDrejtimiId.setText("");
       // txtMesuesjaId.setText("");

    }

}
