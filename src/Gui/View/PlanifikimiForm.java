
package Gui.View;

import BLL.Planifikimi;
import DAL.CrudFormException;
import DAL.PlanifikimiInterface;
import DAL.PlanifikimiRepository;
import Gui.Model.PlanifikimiModel;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;


public class PlanifikimiForm extends javax.swing.JInternalFrame {
    PlanifikimiInterface pi = new PlanifikimiRepository();
    PlanifikimiModel pm= new PlanifikimiModel();
   
    public PlanifikimiForm() {
       initComponents();
         loadTable();
    }
     
    public void loadTable() {
        try {
            List<Planifikimi> lista = pi.findAll();
            pm.add(lista);
            tabela.setModel((TableModel) pi);
            pm.fireTableDataChanged();
        } catch (CrudFormException ene) {
            JOptionPane.showMessageDialog(this, ene.getMessage());
        }
    } 
    private void tabelaSelectedIndexChange() {
        final ListSelectionModel rowSM = tabela.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent Ise) {
                if (Ise.getValueIsAdjusting()) {
                    return;
                }
                ListSelectionModel rowSM = (ListSelectionModel) Ise.getSource();
                int selectedIndex = rowSM.getAnchorSelectionIndex();
                if (selectedIndex > -1) {
                    Planifikimi en = pm.getPlanifikimi(selectedIndex);

                    txtPlanifikimiId.setText(en.getPlanifikimiID() + "");
                    

                }
            }
        });
    }
   
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtplanifikimiid = new javax.swing.JLabel();
        txtPlanifikimiId = new javax.swing.JTextField();
        btnSave = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabela = new javax.swing.JTable();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);

        txtplanifikimiid.setText("PlanifikimiID");

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        tabela.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "PlanifikimiID", "", "", ""
            }
        ));
        jScrollPane1.setViewportView(tabela);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(txtplanifikimiid)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPlanifikimiId, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(171, 171, 171)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnDelete)))
                .addContainerGap(81, Short.MAX_VALUE))
            .addComponent(jScrollPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtplanifikimiid)
                    .addComponent(txtPlanifikimiId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave)
                    .addComponent(btnEdit)
                    .addComponent(btnClear)
                    .addComponent(btnDelete))
                .addGap(50, 50, 50)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(30, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
        int row = tabela.getSelectedRow();
        if (!txtplanifikimiid.getText().equals("")) {
            if (row == -1) {
                Planifikimi en = new Planifikimi();
                en.setPlanifikimiID(Integer.parseInt(txtPlanifikimiId.getText()));
                try {
                    pi.create(en);
                } catch (CrudFormException ene) {
                    JOptionPane.showMessageDialog(this, ene.getMessage());
                }

            } else {
                Planifikimi en = pm.getPlanifikimi(row);
               // en.setPlanifikimiID(Planifikimi) pm.getSelectedItem());

                try {
                    pi.edit(en);
                } catch (CrudFormException ex) {
                    Logger.getLogger(PlanifikimiForm.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            clearField();
            loadTable();
        } else {
            JOptionPane.showMessageDialog(this, "Ju lutem plotesoni fushat obligative (me shenjen *)!");

        }
    }
       public static void main(String args[]) {
    java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PlanifikimiForm().setVisible(true);
            }
        });

        
        
        
        
        
        
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
         // TODO add your handling code here:
        int row = tabela.getSelectedRow();
        if (!txtPlanifikimiId.getText().equals("")) {
            if (row == -1) {
                Planifikimi en = new Planifikimi();
                en.setPlanifikimiID(Integer.parseInt(txtPlanifikimiId.getText()));
               
               
                try {
                    pi.create(en);
                } catch (CrudFormException ene) {
                    JOptionPane.showMessageDialog(this, ene.getMessage());
                }

            } else {
                Planifikimi en = pm.getPlanifikimi(row);
                

                try {
                    pi.edit(en);
                } catch (CrudFormException ex) {
                    Logger.getLogger(PrindiForm.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            clearField();
            loadTable();
        } else {
            JOptionPane.showMessageDialog(this, "Ju lutem plotesoni fushat obligative (me shenjen *)!");

        }
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        // TODO add your handling code here:
         clearField();
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        // TODO add your handling code here:
          int row = tabela.getSelectedRow();
        if (row != -1) {
            Object[] ob = {"Po", "Jo"};
            int i = 
			JOptionPane.showOptionDialog(this, "A dëshironi ta fshini ?", "Fshirja", 
			JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE, null, ob, ob[1]);
            if (i == 0) {
                Planifikimi en = pm.getPlanifikimi(row);
                try {
                    pi.delete(en);
                } catch (CrudFormException ex) {
                    Logger.getLogger(PrindiForm.class.getName()).log(Level.SEVERE, null, ex);
                }
                clearField();
                loadTable();
            } else {
                clearField();
            }
        }else{
            JOptionPane.showMessageDialog(this, "Nuk keni selektuar asgje per te fshire!");
        }

    }//GEN-LAST:event_btnDeleteActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnSave;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabela;
    private javax.swing.JTextField txtPlanifikimiId;
    private javax.swing.JLabel txtplanifikimiid;
    // End of variables declaration//GEN-END:variables

    private void clearField() {
        tabela.clearSelection();
        txtPlanifikimiId.setText("");
        
    }
    
}
