
package DAL;

import BLL.Prindi;
import java.util.List;
import javax.persistence.Query;


public class PrindiRepository extends EntMngClass implements PrindiInterface{
    @Override
    public void create(Prindi en) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(en);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }
    @Override
    public void edit(Prindi en) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(en);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }
     @Override
    public void delete(Prindi en) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(en);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }
     @Override
    public List<Prindi> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Prindi.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }
     @Override
    public Prindi findByID(Integer id) {
        Query query = em.createQuery("SELECT e FROM Prindi e WHERE e.ID = :id");
        query.setParameter("id", id);
        return (Prindi)query.getSingleResult();
    }
}
