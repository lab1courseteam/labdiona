
package DAL;

import BLL.Prindi;
import java.util.List;


public interface PrindiInterface {
    void create(Prindi en) throws CrudFormException;
    void edit(Prindi en) throws CrudFormException;
    void delete(Prindi en) throws CrudFormException;
    List<Prindi> findAll() throws CrudFormException;
    Prindi findByID(Integer ID);
}
