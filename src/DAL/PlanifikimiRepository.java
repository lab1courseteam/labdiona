
package DAL;

import BLL.Planifikimi;
import java.util.List;
import javax.persistence.Query;


public class PlanifikimiRepository extends EntMngClass implements PlanifikimiInterface {
    @Override
    public void create(Planifikimi en) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(en);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }
    @Override
    public void edit(Planifikimi en) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(en);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }
    @Override
    public void delete(Planifikimi en) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(en);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }
     @Override
    public List<Planifikimi> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Planifikimi.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }
    @Override
    public Planifikimi findByID(Integer id) {
        Query query = em.createQuery("SELECT e FROM Planifikimi e WHERE e.ID = :id");
        query.setParameter("id", id);
        return (Planifikimi)query.getSingleResult();
    }
}
