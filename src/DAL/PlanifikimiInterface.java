
package DAL;

import BLL.Planifikimi;
import java.util.List;


public interface PlanifikimiInterface {
    void create(Planifikimi en) throws CrudFormException;
    void edit(Planifikimi en) throws CrudFormException;
    void delete(Planifikimi en) throws CrudFormException;
    List<Planifikimi> findAll() throws CrudFormException;
    Planifikimi findByID(Integer ID);
}
